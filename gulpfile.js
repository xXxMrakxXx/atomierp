var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    cleanCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    del = require('del'),
    image = require('gulp-image'),
    cache = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer'),
    notify = require("gulp-notify");

gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: '../'
        },
        notify: false,
    });
});


gulp.task('common-js', function () {
    return gulp.src([
        'js/common.js',
    ])
        .pipe(concat('common.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('js', ['common-js'], function () {
    return gulp.src([
        'libs/jquery/dist/jquery.min.js',
        'libs/swiper/package/js/swiper.js',
        'js/common.min.js',
    ])
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('scss', function () {
    return gulp.src('scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on("error", notify.onError()))
        .pipe(rename({suffix: '.min', prefix: ''}))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('css'))
        .pipe(browserSync.stream())
});

gulp.task('watch', ['scss', 'js', 'browser-sync'], function () {
    gulp.watch('scss/**/*.scss', ['scss']);
    gulp.watch(['libs/**/*.js', 'js/common.js'], ['js']);
    gulp.watch('../*.php', browserSync.reload);
});

// gulp.task('imagemin', function () {
//     return gulp.src('images/**/*')
//         .pipe(image())
//         .pipe(gulp.dest('dist/images'));
// });

gulp.task('build', ['removedist', 'scss', 'js'], function () {

    var buildCss = gulp.src([
        'css/main.min.css',
    ]).pipe(gulp.dest('dist/css'));

    var buildJs = gulp.src([
        'js/scripts.min.js',
    ]).pipe(gulp.dest('dist/js'));

    var buildFonts = gulp.src([
        'fonts/**/*',
    ]).pipe(gulp.dest('dist/fonts'));

});

gulp.task('removedist', function () {
    return del.sync('dist');
});
gulp.task('clearcache', function () {
    return cache.clearAll();
});

gulp.task('default', ['watch']);
